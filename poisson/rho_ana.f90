!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine rho_ana(x,d,dx)
  use amr_parameters
  use hydro_parameters
  use poisson_parameters
  implicit none
  real(dp)::dx                  ! Cell size
  real(dp)::d                   ! Density
  real(dp),dimension(1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine generates analytical Poisson source term.
  ! Positions are in user units:
  ! x(1:3) are in [0,boxlen]**ndim.
  ! d is the density field in user units.
  !================================================================
  integer::i
  real(dp)::dmass,emass,xmass,ymass,zmass,rr,rx,ry,rz,dd

  emass=gravity_params(1) ! Softening length
  xmass=gravity_params(2) ! Point mass coordinates
  ymass=gravity_params(3)
  zmass=gravity_params(4)
  dmass=1.0/(emass*(1.0+emass)**2)

  rx=0.0d0; ry=0.0d0; rz=0.0d0
  rx=x(1)-xmass
#if NDIM>1
  ry=x(2)-ymass
#endif
#if NDIM>2
  rz=x(3)-zmass
#endif
  rr=sqrt(rx**2+ry**2+rz**2)
  dd=1.0/(rr*(1.0+rr)**2)
  d=MIN(dd,dmass)

end subroutine rho_ana
