test which should cover the correct functioning of the code quite broadly. to recompute the comparison test file, use the given patch with the main ramses code and rename the particles.txt.

to update the diffs (like Makefile), use
diff -c  Makefile ../../bin/Makefile > Makefile_diff_new
